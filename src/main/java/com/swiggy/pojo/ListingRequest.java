package main.java.com.swiggy.pojo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;

/**
 * Created by bhavinchauhan on 3/4/16.
 */

@JsonSerialize
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ListingRequest {
    String eventName;
    String lat;
    String lng;
    int start;
}
