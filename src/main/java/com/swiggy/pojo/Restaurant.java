package main.java.com.swiggy.pojo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;

import java.util.List;
import java.util.Map;

@Builder
@Setter
@Getter
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class Restaurant {
    private int id;
    private String uuid;
    private String name;
    @JsonIgnore
    private String city;
    @JsonIgnore
    private String area;
    private String avg_rating;
    @JsonIgnore
    private String total_ratings;
    private String cloudinaryImageId;
    private List<String> cuisine;
    @JsonIgnore
    private int recommended ;
    @JsonIgnore
    private int costForTwo ;
    private String costForTwoString;
    private int deliveryCharge;
    private int minimumOrder;
    private int opened;
    private String nextOpen;
//    private List<Category> categories;
    private int deliveryTime;
    @JsonIgnore
    private List<String> matched;
    @JsonIgnore
    private List<String> items;
    @JsonIgnore
    private Map<String, String> signatures;
    private String nextOpenMessage;
    @JsonIgnore
    private boolean tmpClosed;
    private String type;
    //Following attributes are ad
    @JsonIgnore
    private String cityState;
    @JsonIgnore
    private String address;
    @JsonIgnore
    private int postalCode;
    @JsonIgnore
    private String latLong;
    @JsonIgnore
    private float cityDeliveryCharge;
    @JsonIgnore
    private float  threshold;
    private String bannerMessage;
    private String locality;
    private Integer parentId;
    @JsonIgnore
    private List<Restaurant> chain;
//    private RestaurantListingDiscountType discountType;
//    private RuleDiscount ruleDiscount;
//    private List<TradeDiscountInfo> tradeCampaignHeaders;

    @JsonCreator
    public Restaurant(@JsonProperty("id") int id,
                      @JsonProperty("name") String name){
        this.id = id;
        this.name = name;
    }
}
