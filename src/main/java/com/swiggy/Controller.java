package main.java.com.swiggy;

import com.fasterxml.jackson.databind.ObjectMapper;
import main.java.com.swiggy.pojo.ListingRequest;
import main.java.com.swiggy.pojo.Restaurant;
import main.java.com.swiggy.services.RestaurantListingService;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;


@ServerEndpoint("/wsapis")
public class Controller {
    //queue holds the list of connected clients
    private static Queue<Session> queue = new ConcurrentLinkedQueue<Session>();
    public static final ObjectMapper objectMapper = new ObjectMapper();
    private static Map<String, Object> listingHashMap = new ConcurrentHashMap<>();
    private static Thread rateThread ; //rate publisher thread

//    static
//    {
//        rateThread = new Thread() {
//            public void run() {
//                DecimalFormat df = new DecimalFormat("#.####");
//                while(true)
//                {
//                    double d = 2 + Math.random();
//                    if(queue != null) {
//                        sendAll("Test");
//                    }
//                    try {
//                        sleep(2000);
//                    } catch (InterruptedException e) {
//                    }
//                }
//            };
//        } ;
//        rateThread.start();
//    }

    @OnMessage
    public void onMessage(Session session, String msg) {
        try {
            Long startTime = System.currentTimeMillis();
//            System.out.println("Start Time:" + startTime);
            ListingRequest request = objectMapper.readValue(msg, ListingRequest.class);
            List<Restaurant> restaurants;
            if (request.getEventName().equals("listing")) {
                System.out.println("Batch id:" + request.getStart());
                if (listingHashMap.get(session.getId()) != null) {
                     restaurants = (List<Restaurant>) listingHashMap.get(session.getId());
                } else {
                    System.out.println("received msg " + msg
                            + " from " + session.getId() + "first request:" + System.currentTimeMillis());
                    RestaurantListingService restaurantListingService = new RestaurantListingService();
                    restaurants = restaurantListingService.getRestaurantsFromDiscountFromService(request.getLat(), request.getLng());
                    if (restaurants != null) {
                        System.out.println("Listing received. Restaurants count:" + restaurants.size());
                        listingHashMap.put(session.getId(), restaurants);
                    }
                }
                if(restaurants == null || restaurants.size() == 0){
                    session.getBasicRemote().sendText(objectMapper.writeValueAsString(null));
                    return;
                }
                int size = restaurants.size();
                int start = request.getStart() * 5;
                int end = (start + 5 > size ? size : start + 5);
                if (start > end) {
                    session.getBasicRemote().sendText(objectMapper.writeValueAsString(null));
                    return;
                }
                session.getBasicRemote().sendText(objectMapper.writeValueAsString(restaurants.subList(start, end)));
            } else {
                session.getBasicRemote().sendText("Test");
            }
            System.out.println("End Time:" + (System.currentTimeMillis() - startTime));
//            Thread.sleep(1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnOpen
    public void open(Session session) throws IOException {
        queue.add(session);
        //            session.getBasicRemote().sendText("Test");
        System.out.println("New session opened: "+session.getId());
    }

    @OnError
    public void error(Session session, Throwable t) {
        queue.remove(session);
        try {
            session.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @OnClose
    public void closedConnection(Session session) {
        queue.remove(session);
        System.out.println("session closed: "+session.getId());
        try {
            session.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void sendAll(String msg) {
        try {
   /* Send the new rate to all open WebSocket sessions */
            ArrayList<Session > closedSessions= new ArrayList<>();
            for (Session session : queue) {
                if(!session.isOpen())
                {
                    System.err.println("Closed session: " + session.getId());
                    closedSessions.add(session);
                }
                else
                {
                    session.getBasicRemote().sendText(msg);
                }
            }
            queue.removeAll(closedSessions);
            System.out.println("Sending "+msg+" to "+queue.size()+" clients");
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}