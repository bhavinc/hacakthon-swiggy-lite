package main.java.com.swiggy.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import main.java.com.swiggy.pojo.Restaurant;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Created by bhavinchauhan on 3/4/16.
 */

/**
 * Created by bhavinchauhan on 12/28/15.
 */

public class RestaurantListingService {

    private static String listingURL = "http://127.0.0.1:8081/api/v1/restaurants?";

    private static final String username = "GGYSWI";
    private static final String password = "2015SW!GGY";
    private static final Integer successfulStatucCode = 0;

    public List<Restaurant> getRestaurantsFromDiscountFromService(String lat, String lng) throws IOException, JSONException {
        int socketConnectionTimeout = 10000;
        RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(500).setConnectTimeout(500).setSocketTimeout(socketConnectionTimeout).build();
        CloseableHttpClient closeableHttpClient = HttpClients.custom().setDefaultRequestConfig(requestConfig).build();
        String url = listingURL + "lat=" + lat + "&" + "lng=" + lng;
        HttpGet httpPost = new HttpGet(url);
        String authHeaderString = username + ":" + password;
        String encodedHeader = Base64.encodeBase64String(authHeaderString.getBytes());
        httpPost.addHeader("Authorization", "Basic " + encodedHeader);
        httpPost.addHeader("content-type", "application/json");
        ObjectMapper objectMapper = new ObjectMapper();
//        httpPost.setEntity(new StringEntity(objectMapper.writeValueAsString(restaurants), ContentType.APPLICATION_JSON));
        CloseableHttpResponse closeableHttpResponse = closeableHttpClient.execute(httpPost);
        if (closeableHttpResponse.getStatusLine().getStatusCode() != 200) {
            System.out.println("Listing Engine returned error:" + closeableHttpResponse.getStatusLine().getStatusCode());
            return null;
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(closeableHttpResponse.getEntity().getContent()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = reader.readLine()) != null) {
            response.append(inputLine);
        }
        ObjectMapper mapper = new ObjectMapper();
        if (!response.toString().isEmpty()) {
            JsonNode jsonNode = mapper.readTree(response.toString());
            if (jsonNode.hasNonNull("statusCode") && jsonNode.get("statusCode").intValue() == 0) {
                TypeReference<List<Restaurant>> typeRef = new TypeReference<List<Restaurant>>() {
                };
                return (List<Restaurant>) mapper.readValue(jsonNode.get("data").get("restaurants").traverse(), typeRef);
            } else {
//                logger.error("Discount Engine returned error:" + response.toString());
            }
        }
        return null;
    }
}